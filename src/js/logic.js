$(document).ready((e) => {
    var windowWidth = $(window).width();
    let cardContainer = $('.cards-container')
    fillProviders()
    if (windowWidth <= 768) {
        $('.nav-link').attr("data-toggle", "collapse");
        $('.nav-link').attr("data-target", "#navbarNav");
        $('#imgEnfoque').removeClass('d-none');
    }

    $('.navbar a').click(function (event) {
        event.preventDefault();
        let id = $($(this)[0]).attr("href"),
            cords = $(id).offset().top,
            min = 50;
        $('html,body').animate({
            scrollTop: cords - min
        }, 'slow');
    });
    
    $('.carousel').carousel({
        interval: 2000
    })
    $('.img-second').parallax({ imageSrc: '../images/img-2.jpg' })
    // logic mail
    $('#form-button').click(function(e){
        e.preventDefault();
        let name = $('#nombre').val(),
            correo = $('#correo').val(),
            asunto = $('#asunto').val(),
            message = $('#mensaje').val();
        if (name == "" || correo == "" || asunto == "" || message == "") {
            $('.alert-danger').removeClass('d-none');
            $('.alert-danger').addClass('show');
        }else{
            sendMail(name,correo,asunto,message);
        }
    });

    $('.alert-danger-button').click(function(e){
        $('.alert-danger').removeClass('show');
        $('.alert-danger').addClass('d-none');
    });
    $('.alert-success-button').click(function (e) {
        $('.alert-success').removeClass('show');
        $('.alert-success').addClass('d-none');
    })
    function sendMail(name,correo,asunto,message) { 
        var template_params = {
            "name": name,
            "email": correo,
            "subject": asunto,
            "message": message
        }
        var service_id = "default_service";
        var template_id = "felox_form";
        emailjs.send(service_id, template_id, template_params).then((e)=>{
            if (e.text == "OK") {
                $('.alert-success').removeClass('d-none');
                $('.alert-success').addClass('show');
            }
        })
        
    }

    function fillProviders(){
        let jsonProviders = [ { img: './src/images/panasonic.jpg', text:'Pilas Panasonic'  },
            { img: './src/images/quala.png', text: 'Quala' }, 
            //{ img: './src/images/henkel.png', text: 'Henkel Colombia' }, 
            { img: './src/images/pfizer.png', text: 'Pfizer' }, 
            { img: './src/images/bayer.png', text: 'Bayer' }, 
            { img: './src/images/dryppers.jpg', text: 'Drypers' }, 
            { img: './src/images/super.jpg', text: 'Superalimentos' }, 
            { img: './src/images/levapan.jpg', text: 'San jorge' }, 
            { img: './src/images/dersa.jpg', text: 'Dersa S.A' }, 
            { img: './src/images/aldor.jpg', text: 'Aldor' }, 
            { img: './src/images/bic.jpg', text: 'Bic' }, 
            { img: './src/images/raza.jpg', text: 'Raza' }, 

        ]

            for (const item in jsonProviders) {
                let cardTemplate = ` <div class="card col-6 col-md-2 m-md-2">
                                <img src="${jsonProviders[item].img}" class="img-fluid">
                                <div class="set-dark"></div>
                                <div class="card-content">
                                    <p>${jsonProviders[item].text}</p>
                                </div>
                            </div>`

                cardContainer.append(cardTemplate)
                
            }
        
    }
})